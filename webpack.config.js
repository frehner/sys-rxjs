const webpack = require('webpack')
const path = require('path');

module.exports = {
  entry: {
    mod1: './src/mod1.js'
  },
  output: {
    filename: '[name].js',
    libraryTarget: 'system',
    path: path.resolve(__dirname),
  },
  mode: 'development',
  externals: [
    /^rxjs\/?.*$/,
  ],
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
  }
}
