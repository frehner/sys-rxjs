# Recreating a bug with rxjs and systemjs

## Steps to reproduce
1. run `npx httpserver`
1. Open a web browser to `http://localhost:8080`
1. Open the console and see that finalize isn't correctly called in every situation - appears to be a race condition. See section below

## Errors received
There are several different errors that can be received, and none of them consistently. Sometimes both `finalize`s are called, sometimes only one is called, and sometimes there's an error and the code isn't even executed.

1. `TypeError: Object(...) is not a function`.
    * Can be found in Firefox v72 on MacOS, and more rarely in Chrome 80 on MacOS
    * Points to this line `Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])((x) => console.log('module', x)),`
1. Mod1 `finalize` is not called
    * Can be found in Firefox v72 on MacOS with `Disable Cache` **unchecked**
    * Can be found in Chrome 80 on MacOS with `Disable Cache` **checked**
    * Can be found in Safari 13.0.4 in both cache situations
1. `Uncaught (in promise) TypeError: rxjs.interval is not a function at (index):46`
    * Can be found in Chrome 80 on MacOS with `Disable Cache` **unchecked**

## Steps to dev
1. `npm i`
1. `npm start`
1. change the import map to use port `8081` for `mod1`
